

export async function getWeatherForm(query: string = 'Guatemala') {
	return await fetch(`/api/get-weather?q=${query}`)
		.then(res => res.json());
}

