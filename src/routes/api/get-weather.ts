const FETCH_OPTIONS = {
	method: "GET",
	headers: {
		"X-RapidAPI-Key":
			"b71dbb3987msh942f756b41229c6p1c5884jsn0695ba0db245",
		"X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com",
	},
};
export async function get(event: any) {
	const { searchParams } = event.url;
	const query = searchParams.get('q') ?? 'Guatemala';

	const response = await fetch(`https://weatherapi-com.p.rapidapi.com/current.json?q=${query}`, FETCH_OPTIONS)
	const data = await response.json()
	const { location, current } = data;
	const { country, localtime, name } = location;
	const {
		condition,
		humidity,
		feelslike_c,
		temp_c,
		wind_kph,
		wind_dir,
		is_day,
	} = current;
	const { icon, text } = condition;

	const body = {
		conditionIcon: icon,
		conditionText: text,
		country,
		localtime,
		name,
		humidity,
		isDay: is_day,
		feelsLike: feelslike_c,
		temperature: temp_c,
		windSpeed: wind_kph,
		windDir: wind_dir,
	}

	console.log(event)
	return {
		status: 200,
		body: body
	}
}
